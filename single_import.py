import xlsxwriter

import config
from extract import extract

if __name__ == '__main__':
    workbook = xlsxwriter.Workbook(config.FILENAME)
    # extract(workbook, (120, 'Arnaud'))
    extract(workbook)
    workbook.close()
