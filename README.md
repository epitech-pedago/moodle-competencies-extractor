# Moodle Competencies Extractor

Small  (not so much anymore) script to extract competencies (acquired and non-acquired) from Epitech Moodle Intranets (Gandalf and Merlin)


There are three main scripts usable in this repository:  
  - `single_import.py` is useful for students, 
    give them this script along with ```extract.py config.py``` 
    and they can extract their own competencies whenever they want
  - `mass_import.py` not so useful anymore but if you want to extract the competencies 
    of only a few students, you can use this script
  - `main.py` the perfect scripts for teachers, you can retrieve all the competencies of every students for a specific city and/or promotion

## Requirements

Python 3.9+ (If you want to use Python 3.6+, you have to remove type hinting from function parameters!!!)

(Recommended) Create a new python venv `python3 -m venv venv` then activate it `source venv/bin/activate`

Install the dependencies `pip3 install -r requirements.txt`

## Config file

Please **do not edit** the `do_not_touch_config.py` file, the name is pretty explicit.

Edit the config file based on your needs :

  - `SESSION_ID` your Moodle Authentication cookie, cookie is name `MoodleSession`
  - `BASE_URL` and `SEMESTER` just uncomment the lines under `# MSC` or `# Digital` based on the intranet you're using 
  - `FILENAME` is the name of the Excel file that'll be created. **It needs to have the .xlsx extension** 
  - `STUDENT_LOGIN_ID` **only for the mass_import.py  script** is a list of tuple id/login. Check the commented example in the config file for the proper syntax
