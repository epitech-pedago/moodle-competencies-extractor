SESSION_ID = ''

# Digital
# BASE_URL = 'https://merlin.epitech.eu/'
# SEMESTER = 'undefined'

# MSC
BASE_URL = 'https://gandalf.epitech.eu/'
SEMESTER = 'all'

FILENAME = "CompetenciesMSC2.xlsx"

# Mass import config
# STUDENT_LOGIN_ID = [
#     (92, 'Spam'),
#     (139, 'Eggs'),
# ]
